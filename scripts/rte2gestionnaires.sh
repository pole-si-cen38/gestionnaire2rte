#!/usr/bin/env bash

#set -x;
# Variables sont enregistrés dans le fichier variable.conf

############################ TEMPLATE DES VARIABLES ######################################
#   pg_host="192.168.0.81"
#   pg_port="5456"
#   pg_user="postgres"
#   pg_password="1234"
#   pg_dbname="postgres"
#   pg_schema_gestionnaires="gestionnaires"
#   pg_schema_rte="rte"
#   dept_rte="38,43"
#   log_file="../logs/test.log"
#   folder_logs="../logs"
#   folder_data_tmp="../datas_tmp"
#   union_xml_rte_convexhull="union_convexhulll_chemin_relatif.xml"
#   red="\e[031m"
#   green="\e[032m"
#   end_color="\e[0m"
########################################################################################


# set -o errexit
# set -o nounset
# set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./script.sh arg-one arg-two

Pipeline to extract data in many sources WFS, zip/shp to postgres database to work with.
test ligne 2

'
    exit
fi

# Permet de connaitre le répertoire d'où est lancé le script

script_directory="$(dirname "${BASH_SOURCE[0]}")"
echo "Le script se trouve dans le répertoire : $script_directory"
cd $script_directory
echo "$PWD"

run_in_directory() {
  local directory="$1"
  shift
  (cd "$directory" && "$@")
}


#cd "$(dirname "$0")"


#HOURS="0"
MIN="0"

function mytimer()
{

#     if [[ "$MIN" -ge "60" ]]
#             then ((HOURS++))
#             MIN="0"
#     fi
 
 
     if [[ "$SECONDS" -ge "60" ]]
             then ((MIN++))
             SECONDS="0"
     fi

 echo -e "${green}Temps écoulé : $MIN minutes : $SECONDS secondes${end_color}"
 
 }

function check_apps()
{

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 1/8 DEBUT : Vérification des programmes" | tee -a "${log_file}"

    ogr2ogr=$(which ogr2ogr)
    ogr2ogr=$(echo $?)
    postgres_test_connexion=$(timeout 2 ogrinfo PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname" 2> /dev/null)
    postgres_test_connexion=$(echo $?)
    #postgres_connection=$(pg_isready -d ${pg_user} -h ${pg_host} -p ${pg_port} -U ${pg_user})
    #postgres_connection=$(echo $?)

    if [[ "$ogr2ogr" -eq 0 ]] && [[ "$postgres_test_connexion" -eq 0 ]]
        then
        #echo $ogr2ogr
        #echo $postgres_test_connexion
        echo "Tous les programmes sont disponibles sur le serveur." >> "${log_file}"
        echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [x] 1/8 FIN : Vérification des programmes" | tee -a "${log_file}"
        echo "" >> "${log_file}"
        mytimer
        else
          #  if [[ "$ogr2ogr" -ne 0 ]]
           # then echo "Il n'y a pas ogr2ogr d'installer"
            
        echo -e "${red}ERREUR : Il manque un programme pour faire fonctionner le script, vérifier la présence de ogr2ogr et la connexion à postgres.${end_color}" | tee -a "${log_file}"
        exit 1
    fi

SECONDS="0"

}


function workspace_clean()
{

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 2/8 DEBUT : Création des dossiers de travail"  | tee -a "${log_file}"

    logs=$(mkdir -p "${folder_logs}"  2>/dev/null)
    datas_out=$(mkdir -p "${folder_datas_out}" 2>/dev/null)
    clean_schema_db=$(timeout 2 ogrinfo -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/0_clean_database.sql 2>/dev/null)
    clean_schema_db=$(echo $?)
    clean_convex_hull=$(rm -f ../datas_out/convex_hull/* ../datas_out/gestionnaires/* ../datas_out/resultats/*)
    clean_convex_hull=$(echo $?)

    if [[ "$clean_convex_hull" -eq 0 ]]
        then
        echo "Clean des schémas dans Postgres"
        else
        echo -e "${red}ERREUR : la fonction n'a pas nettoyer les bases.${end_color}"
    fi
    #ogrinfo -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @../sql/0_clean_database.sql

    if [[ "$logs" -eq 0 ]] && [[ "$datas_out" -eq 0 ]]  && [[ "$clean_schema_db" -eq 0 ]] && [[ "clean_convex_hull" -eq 0 ]]
    then 
    echo -e "${green}Tous les dossiers de travail sont accessible.\nLes schémas rte, gestionnaires et traitements on été nettoyés.${end_color}" | tee -a "${log_file}"

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 2/8 FIN : Création des dossiers ${folder_logs} et ${folder_datas_out}" | tee -a "${log_file}"
    mytimer
    else

        echo -e "${red}ERREUR : Il manque un programme pour faire fonctionner le script, vérifier la présence de ogr2ogr et si la connexion à postgres est opérationnel${end_color}" >> "${log_file}"
        exit 2
    fi

   
        
SECONDS="0"
    
}


function calcul_convex_hull()
{
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 3/8 DEBUT : Création du polygone convexe des couches RTE"  | tee -a "${log_file}"
    ## RTE
    # Convex Hull des couches RTE
    
    #if [ -f "${FOLDER_TO_BACKUP}/${NAME_FILE}" ] ; then
    

    #for i in {38,74,43}; 
    for dept in $departement_rte
        do ogr2ogr -a_srs EPSG:2154 -append ../datas_out/convex_hull/convex_hull.shp "xml/rte/${dept}_rte_${union_xml_rte_convexhull}" 2> "../datas_out/logs/1_debug_convexhull.txt" 
        done
    
    mytimer
    echo "Calcul de l'enveloppe convexe de chaque type de géometrie pour les données RTE"
    SECONDS="0"
    
    #ogr2ogr -a_srs EPSG:2154 -append ../datas_tmp/convex_hull.shp ../xml/union_2layers_sql.xml 2> ../logs/1_debug_convexhull.txt
    echo "Réalisation des enveloppes convexes globale des 3 types géometrique"
    mytimer
    SECONDS="0"

    #for i in {38,74}; 
    # Union des enveloppes externes
    ogr2ogr -a_srs EPSG:2154 -append ../datas_out/convex_hull/union_convex_hull.shp ../datas_out/convex_hull/convex_hull.shp -dialect SQLite -sql "SELECT ST_ConvexHull(ST_Collect(GEOMETRY)) FROM convex_hull"
    #done
    
    
    echo "Réalisation de l'enveloppe convexe des sites RTE."
            
    mytimer
    SECONDS="0"
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 3/8 FIN : Création du polygone convexe des couches RTE"  | tee -a "${log_file}"
}


function recuperation_donnees_gestionnaires()
{
    
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 4/8 DEBUT : Récupérer les flux de données \"gestionnaires\"."  | tee -a "${log_file}"
    # Récupérer l'enveloppe convexe (CONVEXHULL) des sites RTE pour ne pas télécharger toute la france.
    # Modifier des xml avec la valeur du CONVEXHULL précédement calculé dans la balise <SrcRegion clip="true">
    CONVEXHULL="$(ogrinfo -al ../datas_out/convex_hull/union_convex_hull.shp | grep "POLYGON" | sed 's/^[ \t]*//' )"

    sed -i -e "s/POLYGON.*))/\\${CONVEXHULL}/g" xml/gestionnaires/test/liste_23_gestionnaires_flux_filtre_field_clip-convexhull.xml 2> ../datas_out/logs/2_debug_union_convexhull_rte.txt;

    
    ## GESTIONNAIRES
    # Récupération des flux et conversions en shape
    # Pour débugger, utiliser l'option 
    #  --debug ON 
    ogr2ogr -overwrite -a_srs EPSG:2154 -lco GEOMETRY_NAME=geom -f "ESRI Shapefile" ../datas_out/gestionnaires xml/liste_23_gestionnaires_flux_filtre_field_clip-convexhull.xml -lco SPATIAL_INDEX=YES 2> ../datas_out/logs/3_debug_flux_gestionnaires.txt
    echo "Récupération des flux et conversions en shape"
    mytimer
    SECONDS="0"
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 4/8 FIN : Récupérer les flux de données \"gestionnaires\"."  | tee -a "${log_file}"
    
    
}

function recuperation_donnees_gestionnaires_xargs()
{
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 4/8 DEBUT : Récupérer les flux de données \"gestionnaires\"."  | tee -a "${log_file}"

    CONVEXHULL="$(ogrinfo -al ../datas_out/convex_hull/union_convex_hull.shp | grep "POLYGON" | sed 's/^[ \t]*//' )"

    find xml/gestionnaires -maxdepth 1 -name '*.xml' -print0 | xargs -0 -P 8 -I {} sed -i -e "s/POLYGON.*))/\\${CONVEXHULL}/g" {}
    
    ## GESTIONNAIRES
    # Récupération des flux via paralelisation des processeur et conversions en shape
    find xml/gestionnaires -maxdepth 1 -name '*.xml' -print0 | xargs -0 -P 8 -I {} ogr2ogr -overwrite -a_srs EPSG:2154 --debug ON -lco GEOMETRY_NAME=geom -f "ESRI Shapefile" ../datas_out/gestionnaires {} -lco SPATIAL_INDEX=YES 2> ../datas_out/logs/test.txt
    
    # Récupération des flux et aggrégation en gpkg
    #find xml/gestionnaires -maxdepth 1 -name '*.xml' -print0 | xargs -0 -P 8 -I {} ogr2ogr -append -update -a_srs EPSG:2154 --debug ON -lco GEOMETRY_NAME=geom -f GPKG ../datas_out/gestionnaires/test_gpkg {} -nlt PROMOTE_TO_MULTI -lco SPATIAL_INDEX=YES 2> ../datas_out/logs/test.txt

    # Conversion des xml en shp via une boucle for
    #  for xml in xml/gestionnaires/*.xml; do
    # Pour débugger, utiliser l'option 
    #  --debug ON 
    #  ogr2ogr -append -update -a_srs EPSG:2154 -lco GEOMETRY_NAME=geom -f GPKG ../datas_out/gestionnaires/test_gpkg $xml -nlt PROMOTE_TO_MULTI -lco SPATIAL_INDEX=YES
    #  done




    mytimer
    SECONDS="0"
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 4/8 FIN : Récupérer les flux de données \"gestionnaires\"."  | tee -a "${log_file}"
}

function encoding_to_utf8 {

    # Convert dbf to csv
    for file in ../datas_in/gestionnaires/*.shp; do
    base=$(basename "$file" .shp)
    ogr2ogr -progress -f "ESRI Shapefile" -lco ENCODING=UTF-8 ../datas_in/gestionnaires/"$base"_utf8.shp ../datas_in/gestionnaires/"$base".shp
    done
    # # Get encoding of csv file
    # encoding=$(file -i ../datas_in/gestionnaires/"$base".csv | cut -d '=' -f 2)
# 
    # if [ "$encoding" != "utf-8" ]
    # then
    #     # Convert encoding to UTF-8
    #     iconv -f "$encoding" -t utf-8 ../datas_in/gestionnaires/"$base".csv > ../datas_in/gestionnaires/"$base"_utf8.csv
# 
    #     # Convert csv to shp with UTF-8 encoding
    #     ogr2ogr -f "ESRI Shapefile" "$base".shp ../datas_in/gestionnaires/"$base"_utf8.csv -lco ENCODING=UTF-8
    # else
    #     # Convert csv to shp with UTF-8 encoding
    #     ogr2ogr -f "ESRI Shapefile" ../datas_in/gestionnaires/"$base".shp ../datas_in/gestionnaires/"$base".csv -lco ENCODING=UTF-8
    # fi
}

function transfers_shape_2_postgres()
{

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 5/8 DEBUT : Transferts données \"RTE\" vers Postgres."  | tee -a "${log_file}"   

    ## RTE
    
    find ../datas_in/rte -name '*.zip' -print0 | xargs -0 -P 8 -I {} ogr2ogr -overwrite -lco GEOMETRY_NAME=geom -a_srs EPSG:2154 -progress -lco PG_USE_COPY=YES -gt 65536 -f "PostgreSQL" PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname} active_schema=${pg_schema_rte}" /vsizip/{} -nlt PROMOTE_TO_MULTI -lco  precision=NO -skipfailures 2>> ../datas_out/logs/4_zipshp2pg_rte_debug.txt   

    # Même script mais avec boucle for
    # echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 5/7 DEBUT : Transferts LOOP FOR données \"RTE\" vers Postgres."  | tee -a "${log_file}"    

    #      for i in $departement_rte  
    #          do
    #          echo "Transfert des données RTE-$i vers Postgres"
    #          ogr2ogr -overwrite --debug ON -lco GEOMETRY_NAME=geom -a_srs EPSG:2154 -progress -lco PG_USE_COPY=YES -gt 65536 -f "PostgreSQL" PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname} active_schema=${pg_schema_rte}" /vsizip/../datas_in/RTE_"$i".zip -nlt PROMOTE_TO_MULTI -lco  precision=NO -skipfailures 2> ../logs/4_zipshp2pg_rte_"$i"_debug.txt
    #          echo "Transfert des données RTE-$i vers Postgres OK"
    #      done
    # echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 5/7 DEBUT : Transferts LOOP FOR données \"RTE\" vers Postgres."  | tee -a "${log_file}"    

    mytimer
    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 5/8 FIN  : Transferts données \"RTE\" vers Postgres."  | tee -a "${log_file}"  

    SECONDS="0" 

        # GESTIONNAIRES
         

        echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 6/8 DEBUT : Transferts données \"gestionnaires\" vers Postgres."  | tee -a "${log_file}"

        find ../datas_out/gestionnaires -name '*.shp' -print0 | xargs -0 -P 8 -I {} ogr2ogr -a_srs EPSG:2154 -overwrite -lco GEOMETRY_NAME=geom -gt 65536 -f "PostgreSQL" PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname} active_schema=${pg_schema_gestionnaires}" {} -nlt MULTIPOLYGON -lco precision=NO -skipfailures 2>> ../datas_out/logs/5_debug_shp2pg_gestionnaires.txt 

        # Même Sript que dessus mais avec boucle for
        #TOTAL_SECONDS=0
        #echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 6/7 DEBUT : Transferts LOOP FOR données \"gestionnaires\" vers Postgres."  | tee -a "${log_file}"  
#
        # for file in ../datas_in/gestionnaires/*.shp
        # do 
        #     [[ -e "$file" ]] || break # sécurité, permet de sortir de la boucle quand il n'y a pas de fichier
        #     # ogr2ogr --debug ON -a_srs EPSG:2154 -overwrite -lco GEOMETRY_NAME=geom -lco SCHEMA=geoserver -f "PostgreSQL" PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname} active_schema=${pg_schema_gestionnaires}" "$file" -nlt MULTIPOLYGON -lco precision=NO -skipfailures 2> ../logs/5_debug_shp2pg_gestionnaires.txt
        #     ogr2ogr --debug ON -a_srs EPSG:2154 -overwrite -lco GEOMETRY_NAME=geom -lco PG_USE_COPY=YES -gt 65536 -f "PostgreSQL" PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname} active_schema=${pg_schema_gestionnaires}" "$file" -nlt MULTIPOLYGON -lco precision=NO -skipfailures 2> ../logs/5_debug_shp2pg_gestionnaires.txt
#
        #     echo "${SECONDS}  secondes pour transférer la donnée ${file}"
        #     TOTAL_SECONDS=$(expr ${TOTAL_SECONDS} + ${SECONDS})
        #     SECONDS="0"
        # done
        #echo "${TOTAL_SECONDS} secondes : pour transferer les données \"gestionnaires\"."   
#
        #echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 6/7 FIN  : Transferts LOOP FOR données \"gestionnaires\" vers Postgres."  | tee -a "${log_file}"   

        mytimer
        SECONDS="0"
        echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 6/8 FIN : Transfert des couches de travail dans Postgres."  | tee -a "${log_file}"
}


function execute_sql()
{
echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 7/8 DEBUT : Lancer les scripts sql."  | tee -a "${log_file}"

# for dept in {38,43} # Liste des départemnt
#for dept in {38,74,43}
    for dept in $departement_rte
        do
            for type in {pln,ppt,psf};  # Liste des types géométriques
                do
                        if [ "$dept" -eq 43 ] # Si le département = 43 alors on effectue un sed
                            then
                                #echo "dept$dept est égale à 43 et $type"
                                sed -i -e "s/\(23\|23_24\)/23/g" sql/1_site2gestionnaires.sql
                                #echo "sed 0a"

                            else 
                                #echo "dept$dept est édifférent à 43 et $type"
                                sed -i -e "s/\(23\|23_24\)/23_24/g" sql/1_site2gestionnaires.sql
                                #echo "sed 0b"
                        fi

                        #echo "sed 1"
                      sed -i -e "s/\(38\|43\|74\)/$dept/g" sql/1_site2gestionnaires.sql
                      sed -i -e "s/\(pln\|ppt\|psf\)/$type/g" sql/1_site2gestionnaires.sql #> changed_"$dept"_"$i".txt
                         #echo "sed 2"

                        ogrinfo -q -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/1_site2gestionnaires.sql | tee -a "${log_file}"
                done
    done 



    #for dept in {38,74,43}
    for dept in $departement_rte
        do
            for i in {pln,ppt,psf};  
                do
                        if [ "$dept" -eq 43 ] # Si le département = 43 alors on effectue un sed
                            then
                                #echo "dept$dept est égale à 43 et $i"
                                sed -i -e "s/\(23\|23_24\)/23/g" sql/2_listing_gestionnaires.sql
                                #echo "sed 1a"

                            else 
                                #echo "dept$dept est édifférent à 43 et $i"
                                sed -i -e "s/\(23\|23_24\)/23_24/g" sql/2_listing_gestionnaires.sql
                                #echo "sed 1b"
                        fi

                        sed -i -e "s/\(38\|43\|74\)/$dept/g" sql/2_listing_gestionnaires.sql
                        sed -i -e "s/\(pln\|ppt\|psf\)/$i/g" sql/2_listing_gestionnaires.sql

                        ogrinfo -q -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/2_listing_gestionnaires.sql | tee -a "${log_file}"
            done
    done


     #for dept in {38,74,43}
    for dept in $departement_rte
        do
            for i in {pln,ppt,psf};  
                do
                        if [ "$dept" -eq 43 ] # Si le département = 43 alors on effectue un sed
                            then
                                #echo "dept$dept est égale à 43 et $i"
                                sed -i -e "s/\(23\|23_24\)/23/g" sql/4_synthese_resultats.sql
                                #echo "sed 1a"

                            else 
                                #echo "dept$dept est édifférent à 43 et $i"
                                sed -i -e "s/\(23\|23_24\)/23_24/g" sql/4_synthese_resultats.sql
                                #echo "sed 1b"
                        fi

                        sed -i -e "s/\(38\|43\|74\)/$dept/g" sql/4_synthese_resultats.sql
                        sed -i -e "s/\(pln\|ppt\|psf\)/$i/g" sql/4_synthese_resultats.sql

                        ogrinfo -q -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/4_synthese_resultats.sql | tee -a "${log_file}"
            done
    done



    #for dept in {38,74,43}
    for dept in $departement_rte

    do
        sed -i -e "s/\(38\|43\|74\)/$dept/g" sql/3_listing_all_gestionaires.sql
        ogrinfo -q -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/3_listing_all_gestionaires.sql | tee -a "${log_file}"
    done

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 7/8 FIN  : Lancer les scripts sql."  | tee -a "${log_file}"

    #for dept in {38,74,43}
    for dept in $departement_rte

    do
        sed -i -e "s/\(38\|43\|74\)/$dept/g" sql/5_union_synthese_resultats.sql
        ogrinfo -q -al -so PG:"host=${pg_host} port=${pg_port} user=${pg_user} password=${pg_password} dbname=${pg_dbname}" -sql @sql/5_union_synthese_resultats.sql | tee -a "${log_file}"
    done

    echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 7/8 FIN  : Lancer les scripts sql."  | tee -a "${log_file}"

}

function export_resultat_to_gpkg()
{

echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [ ] 8/8 DEBUT : Export des résultats vers fichier GPKG par département."  | tee -a "${log_file}"


# --config PG_LIST_ALL_TABLES YES pour afficher les tables non géographiques
#schema_tables=$(ogrinfo --config PG_LIST_ALL_TABLES YES PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname"  | grep "traitements" | cut -d " " -f2)
#echo "$schema_tables"
#tables=$(ogrinfo --config PG_LIST_ALL_TABLES YES PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname"  | grep "traitements" | cut -d " " -f2 | cut -d "." -f2)
#echo "$tables"
#list_tables=$(ogrinfo --config PG_LIST_ALL_TABLES YES PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname"  | grep "traitements" | cut -d " " -f2 | cut -d "." -d " " -f2 | sed 's/.*/&/;$!s/$/, /' | tr -d '\n')
#echo "$list_tables"

for dept in $departement_rte
    do
    echo $dept
        #ogr2ogr -overwrite -progress -f "ESRI Shapefile" "$tables".shp PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname" -sql "SELECT * FROM "traitements.$tables"" -nln testing_ogr
        #ogr2ogr -append -progress -f GPKG test.gpkg PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname" -sql "SELECT * FROM "traitements.$tables"" -nln "$tables" -lco LAUNDER=NO
        ogr2ogr -q -f GPKG ../datas_out/resultats/cen"$dept"_extraction_rte_gestionnaires.gpkg PG:"host=$pg_host port=$pg_port user=$pg_user password=$pg_password dbname=$pg_dbname tables='traitements.rte_pln_intersects_cen$dept,traitements.rte_ppt_intersects_cen$dept,traitements.rte_psf_intersects_cen$dept,traitements.listing_gestionnaires_pln_cen$dept,traitements.listing_gestionnaires_psf_cen$dept,traitements.listing_gestionnaires_ppt_cen$dept,traitements.listing_gestionnaires_all_cen$dept,traitements.rte_synthese_pln_intersects_cen$dept,traitements.rte_synthese_psf_intersects_cen$dept,traitements.rte_synthese_ppt_intersects_cen$dept,traitements.synthese_listing_gestionnaires_all_cen$dept'" #| tee -a "${log_file}"
        #        ogr2ogr -q -f GPKG ../datas_out/resultats/cen38_extraction_rte_gestionnaires.gpkg PG:"host=192.168.16.100 port=5415 user=postgres password=1234 dbname=postgres tables='traitements.rte_pln_intersects_cen$dept,traitements.rte_ppt_intersects_cen$dept,traitements.rte_psf_intersects_cen$dept,traitements.listing_gestionnaires_pln_cen$dept,traitements.listing_gestionnaires_psf_cen$dept,traitements.listing_gestionnaires_ppt_cen$dept,traitements.listing_gestionnaires_all_cen$dept,traitements.rte_synthese_pln_intersects_cen$dept,traitements.rte_synthese_psf_intersects_$dept,traitements.rte_synthese_ppt_intersects_$dept,traitements.synthese_listing_gestionnaires_all_cen_$dept'" #| tee -a "${log_file}"

    done

echo "[$(date +%d/%m/%Y-%H:%M:%S)] - [X] 8/8 FIN  : Export des résultats vers fichier GPKG par département."  | tee -a "${log_file}"
}

function notification()
{
curl -d "Sauvegarde terminée cd temp/ntfy 😀" ntfy.sh/michmuch3856
}


function main()
{
    # shellcheck source=./scriptsvariable.conf
    source variables.conf
    # Set the start time
    echo -e "${green}[$(date +%d/%m/%Y-%H:%M:%S)] - [ STARTED ] .${end_color}"  | tee -a "${log_file}"
    start_time=$(date +%s)
        #run_in_directory /path/to/desired/directory ./your_script.sh
        check_apps
        workspace_clean
        calcul_convex_hull
        recuperation_donnees_gestionnaires_xargs
        transfers_shape_2_postgres
        execute_sql
        ##test_while_and_for
        export_resultat_to_gpkg
        echo -e "${green}[$(date +%d/%m/%Y-%H:%M:%S)] - [ THE END ].${end_color}"  | tee -a "${log_file}"
        # Set the end time
end_time=$(date +%s)

# Calculate elapsed time
elapsed_time=$((end_time - start_time))

echo "Elapsed time: $elapsed_time seconds"

}
#source variables.conf
main

#execute_sql
#export_resultat_to_gpkg
#check_apps
#workspace_clean
#calcul_convex_hull
#recuperation_donnees_gestionnaires
#test_recupflux_gestionnaires
#test
#encoding_to_utf8
#main
#notification
#calcul_convex_hull
#        recuperation_donnees_gestionnaires_xargs
