#! /bin/bash
# Variables

host="localhost"
port="5415"
user="postgres"
password="1234"
dbname="postgres"
schema_gestionnaires="gestionnaires"
schema_rte="rte"
#HOURS="0"
MIN="0"

function mytimer()
{

#     if [[ "$MIN" -ge "60" ]]
#             then ((HOURS++))
#             MIN="0"
#     fi
 
 
     if [[ "$SECONDS" -ge "60" ]]
             then ((MIN++))
             SECONDS="0"
     fi

 echo "Temps écoulé : $MIN minutes : $SECONDS secondes"
 
 }


## RTE
# Convex Hull des couches RTE
ogr2ogr -a_srs EPSG:2154 -makevalid -append ../datas_tmp/convex_hull.shp ../xml/union_2layers_sql.xml 2> ../logs/1_debug_convexhull.txt
echo "etape 1 : Récupérer l'enveloppe min (Convex Hull) des couches RTE" 
mytimer
SECONDS="0"
# Union des enveloppes externes
ogr2ogr -a_srs EPSG:2154 -append ../datas_tmp/union_convex_hull.shp ../datas_tmp/convex_hull.shp -dialect SQLite -sql "SELECT ST_ConvexHull(ST_Collect(GEOMETRY)) FROM convex_hull" 2> ../logs/2_debug_union_convexhull_rte.txt
echo "etape 2 : Union des enveloppes externes des couches RTE"
mytimer
SECONDS="0"

## GESTIONNAIRES
# Récupération des flux et conversions en shape
ogr2ogr -append -a_srs EPSG:2154 --debug ON -lco GEOMETRY_NAME=geom -makevalid -nlt MULTIPOLYGON -f "ESRI Shapefile" ../datas/gestionnaires ../xml/liste_23_gestionnaires_flux_filtre_field_clip-convexhull.xml -lco SPATIAL_INDEX=YES 2> ../logs/3_debug_flux_gestionnaires.txt
echo "etape 3 : Récupération des flux et conversions en shape"
mytimer
SECONDS="0"
## Transfers shape vers Postgres
# RTE
ogr2ogr -append --debug ON -lco GEOMETRY_NAME=geom -a_srs EPSG:2154 -overwrite -progress -f "PostgreSQL" PG:"host=$host port=$port user=$user password=$password dbname=$dbname active_schema=$schema_rte" /vsizip/../datas/RTE.zip -nlt PROMOTE_TO_MULTI -lco  precision=NO -skipfailures 2> ../logs/4_zipshp2pg_rte_debug.txt
echo "etape 4 : Transfers shape RTE vers Postgres"
mytimer
SECONDS="0"
# GESTIONNAIRES
for file in `ls ../datas/gestionnaires/*.shp`
do ogr2ogr --debug ON -a_srs EPSG:2154 -overwrite -lco GEOMETRY_NAME=geom -progress -f "PostgreSQL" PG:"host=$host port=$port user=$user password=$password dbname=$dbname active_schema=$schema_gestionnaires" $file -nlt MULTIPOLYGON -lco precision=NO -skipfailures 2> ../logs/5_debug_shp2pg_gestionnaires.txt
done
echo "etape 5 : Transfers shape Gestionnaires vers Postgres"
mytimer