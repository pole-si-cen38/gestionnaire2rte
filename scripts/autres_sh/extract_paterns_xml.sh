#!/usr/bin/env bash

#!/bin/bash

# Set the input and output file names
input_file="../xml/liste_23_gestionnaires_flux_filtre_field_clip-bbox-copie.xml"
output_dir="../xml/output"
balise="OGRVRTLayer"

# Make sure the output directory exists
mkdir -p "$output_dir"

# Extract the text between the start and end patterns
text=$(sed -n 's/.*<OGRVRTLayer\(.*\)<\/OGRVRTLayer>.*/\1/p' $input_file)

# Iterate over the extracted text and save each occurrence to a separate file
i=1
while read -r line; do
    echo "$line" > "text-$i.txt"
    ((i++))
done <<< "$text"