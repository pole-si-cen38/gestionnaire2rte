#!/usr/bin/env bash

#!/bin/bash

# Set the input and output file names
input_file="../xml/liste_23_gestionnaires_flux_filtre_field_clip-bbox-copie.xml"
output_dir="../xml/output"

# Make sure the output directory exists
mkdir -p "$output_dir"

# Find all the matching elements
while read -r element; do
  # Extract the element name from the tag
  element_name=$(echo "$element" | grep -oP 'name="\K[^"]+')
  echo $element
  # Create a new file for the element
  output_file="$output_dir/$element_name.xml"
  # Write the element to the file
  echo "$element" | awk -v RS='</OGRVRTLayer>' '{print $0}' | awk '/<OGRVRTLayer/{flag=1;next}/<\/OGRVRTLayer>/{flag=0}flag' > "$output_file"
done < <(awk '/<OGRVRTLayer/' "$input_file")