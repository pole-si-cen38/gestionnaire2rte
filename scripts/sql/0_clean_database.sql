-- Fait le ménage dans les schémas
DROP SCHEMA IF EXISTS rte CASCADE;
DROP SCHEMA IF EXISTS gestionnaires CASCADE;
DROP SCHEMA IF EXISTS traitements CASCADE;
CREATE SCHEMA rte;
CREATE SCHEMA gestionnaires;
CREATE SCHEMA traitements;


COMMENT ON SCHEMA rte IS 'Ensemble des données d''intervention transmis par RTE';
COMMENT ON SCHEMA gestionnaires IS 'Ensemble des données "gestionnaires" récupéré via flux ou couches nationales
Parcs_Naturels_regionaux
Parcs_Nationaux
Znieff2
Znieff1
Arretes_de_protection_de_biotope
Reserves_naturelles_nationales
Reserves_naturelles_regionales
Reserves_nationales_de_chasse_et_faune_sauvage
CEN_MU
Reserve_biologique
Forets_publique
Parcelles_forets_publique
Terrains_acquis_des_Conservatoires_des_espaces_naturels
ZICO
N2000_ZPS
N2000_ZSC
EPCI
Ign_communes
--------ISERE-----
zones_humides
Pelouses sèches
ENS
 ';