DROP TABLE IF EXISTS traitements.synthese_listing_gestionnaires_all_cen38;
CREATE TABLE traitements.synthese_listing_gestionnaires_all_cen38 as
(
SELECT * FROM
	(
SELECT * FROM traitements.rte_synthese_pln_intersects_cen38
UNION ALL
SELECT * FROM traitements.rte_synthese_ppt_intersects_cen38
UNION ALL
SELECT * FROM traitements.rte_synthese_psf_intersects_cen38
	) a
--Group by a.espace, a.id, a.nom, a.gest_site
--ORDER BY a.espace
)