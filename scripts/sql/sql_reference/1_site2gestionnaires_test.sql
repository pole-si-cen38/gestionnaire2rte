--SELECT ST_SRID(geom) FROM gestionnaires.arretes_de_protection_de_biotope LIMIT 1;-- 900914
--SELECT ST_SRID(geom) FROM gestionnaires.cen_mu LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM gestionnaires.epci LIMIT 1;--900915
--SELECT ST_SRID(geom) FROM gestionnaires.n2000_sic LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM gestionnaires.zones_humides_isere LIMIT 1;--900914

DROP TABLE IF EXISTS traitements.rte_psf_intersects_cen38 43 38 43 38 43 38 43;
CREATE TABLE traitements.rte_psf_intersects_cen38 43 38 43 38 43 38 43 as
(
with
		pnr as
		(
		SELECT a.objectid, b.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.parcs_naturels_regionaux b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		 ),
		 pn as
		 (
		SELECT a.objectid, c.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.parcs_nationaux c
		WHERE a.geom && c.geom AND ST_Intersects(a.geom, c.geom)	 
		 ),
		 znieff2 as
		 (
		SELECT a.objectid, d.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.znieff2 d
		WHERE a.geom && d.geom AND ST_Intersects(a.geom, d.geom)	 
		 ) ,
		Znieff1 as
		 (
		SELECT a.objectid, e.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.znieff1 e
		WHERE a.geom && e.geom AND ST_Intersects(a.geom, e.geom)	 
		 ),
		appb as
		 (
		SELECT a.objectid, f.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.arretes_de_protection_de_biotope f
		WHERE a.geom && f.geom AND ST_Intersects(a.geom, f.geom)	 
		 ) ,
		rnn as
		 (
		SELECT a.objectid, g.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.reserves_naturelles_nationales g
		WHERE a.geom && g.geom AND ST_Intersects(a.geom, g.geom)	 
		 ),
		rnr as
		 (
		SELECT a.objectid, h.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.reserves_naturelles_regionales h
		WHERE a.geom && h.geom AND ST_Intersects(a.geom, h.geom)	 
		 ),
		 Reserve_biologique as
		 (
		SELECT a.objectid, i.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.reserve_biologique i
		WHERE a.geom && i.geom AND ST_Intersects(a.geom, i.geom)	 
		 ),
		 cen_mu as
		 (
		SELECT a.objectid, j.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.cen_mu j
		WHERE a.geom && j.geom AND ST_Intersects(a.geom, j.geom)	 
		 ),
		ens_zo1 as
		 (
		 SELECT a.objectid, k.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.ens_zo_01 k
		WHERE a.geom && k.geom AND ST_Intersects(a.geom, k.geom)	
		 ),
		 ens_zo2 as
		 (
		 SELECT a.objectid, l.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.ens_zo_02 l
		WHERE a.geom && l.geom AND ST_Intersects(a.geom, l.geom)	
		 ),
		zico as
		(	
		 SELECT a.objectid, m.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.zico m
		WHERE a.geom && m.geom AND ST_Intersects(a.geom, m.geom)
		),
			n2000_sic as
		(	
		 SELECT a.objectid, n.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.n2000_sic n
		WHERE a.geom && n.geom AND ST_Intersects(a.geom, n.geom)
		),
			n2000_zps as
		(	
		 SELECT a.objectid, o.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.n2000_zps o
		WHERE a.geom && o.geom AND ST_Intersects(a.geom, o.geom)
		),
			zh as
		(	
		 SELECT a.objectid, p.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.zones_humides_isere p
		WHERE a.geom && p.geom AND ST_Intersects(a.geom, p.geom)
		),
			ps as
		(	
		 SELECT a.objectid, q.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.pelouses_seches q
		WHERE a.geom && q.geom AND ST_Intersects(a.geom, q.geom)
		),
			rncfs as
		(	
		 SELECT a.objectid, r.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.reserves_nationales_de_chasse_et_faune_sauvage r
		WHERE a.geom && r.geom AND ST_Intersects(a.geom, r.geom)
		),
			epci as
		(	
		 SELECT a.objectid, s.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.epci s
		WHERE a.geom && s.geom AND ST_Intersects(a.geom, s.geom)
		),
			onf_forets_publique as
		(	
		SELECT a.objectid, t.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.forets_publique t
		WHERE a.geom && t.geom AND ST_Intersects(a.geom, t.geom)
		),
		Parcelles_forets_publique as 		
		(	
		SELECT a.objectid, u.*
		FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a, gestionnaires.parcelles_forets_publique u
		WHERE a.geom && u.geom AND ST_Intersects(a.geom, u.geom)
		)


SELECT
a.objectid,
b.id as pnr_id, b.nom as pnr_nom, b.gest_site as pnr_gest_site, -- PNR
c.id as pn_id, c.nom as pn_nom, -- PN
d.id as znieff2_id, d.nom as znieff2_nom, -- znieff2
e.id as znieff1_id, e.nom znieff1_nom, -- znieff1
f.id as appb_id, f.nom as appb_nom, -- appb
g.id as rnn_id, g.nom as rnn_nom , -- rnn
h.id as rnr_id, h.nom as rnr_nom, h.gest_site as rnr_gest_site, -- rnr
i.id as rb_id, i.nom as rb_nom, -- Reserve biologique
j.id as cen_id , j.nom as cen_nom, j.gest_site as cen_gest_site, -- CEN MU
k.id as ens_z01_id, k.nom as ens_z01_nom, -- ENS_ZO1
l.id as ens_z02_id, l.nom as ens_z02_nom, l.gest_site as ens_z02_gest_site, --ENS_ZO2
m.id as zico_id, m.nom as zico_nom, --ZICO
n.id as n2000_sic_id , n.nom as n2000_sic_nom, --n2000 sic
o.id as n2000_zps_id, o.nom as n2000_zps_nom, -- n2000_zps
p.ogc_fid as zh_id,-- p.nom as zh_site_nom, -- zh
q.ogc_fid as ps_id, --q.nom as ps_nom, -- ps
r.id as rncfs_id, r.nom as rncfs_nom, r.gest_site as rncfs_gest_site, -- RNCFS
s.id as epci_id, s.nom as epci_nom, s.gest_site as epci_gest_site, -- EPCI
t.id as foret_pub_id, t.nom as foret_pub_nom, t.gest_site as foret_pub_gest_site, -- Foret publique
u.id as parcel_foret_pub_id, u.nom as parcel_foret_pub_nom, u.gest_site as parcel_foret_pub_gest_site, -- Parcelle foret publique
a.geom

FROM rte.rte_psf_23_24_cen38 43 38 43 38 43 38 43 a
LEFT JOIN pnr b ON a.objectid = b.objectid
LEFT JOIN pn c ON a.objectid = c.objectid
LEFT JOIN  znieff2 d ON a.objectid = d.objectid
LEFT JOIN  znieff1 e ON a.objectid = e.objectid
LEFT JOIN appb f ON a.objectid = f.objectid
LEFT JOIN rnn g ON a.objectid = g.objectid
LEFT JOIN rnr h ON a.objectid = h.objectid
LEFT JOIN Reserve_biologique i ON a.objectid = i.objectid
LEFT JOIN cen_mu j ON a.objectid = j.objectid
LEFT JOIN ens_zo1 k ON a.objectid = k.objectid
LEFT JOIN ens_zo2 l ON a.objectid = l.objectid
LEFT JOIN zico m ON a.objectid = m.objectid
LEFT JOIN n2000_sic n ON a.objectid = n.objectid
LEFT JOIN n2000_zps o ON a.objectid = o.objectid
LEFT JOIN zh p ON a.objectid = p.objectid
LEFT JOIN ps q ON a.objectid = q.objectid
LEFT JOIN rncfs r ON a.objectid = r.objectid
LEFT JOIN epci s ON a.objectid = s.objectid
LEFT JOIN onf_forets_publique t ON a.objectid = t.objectid
LEFT JOIN Parcelles_forets_publique u ON a.objectid = u.objectid


WHERE
b.id is not null OR
c.id is not null OR
d.id is not null OR
e.id is not null OR
f.id is not null OR
g.id is not null OR 
h.id is not null OR
i.id is not null OR
j.id is not null OR
k.id is not null OR
l.id is not null OR
m.id is not null OR
n.id is not null OR
o.id is not null OR
p.ogc_fid is not null OR
p.ogc_fid is not null OR
r.id is not null OR
s.id is not null OR
t.id is not null OR
u.id is not null
)