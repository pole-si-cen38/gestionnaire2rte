DROP TABLE IF EXISTS traitements.listing_gestionnaires_all;
CREATE TABLE traitements.listing_gestionnaires_all as
(
SELECT * FROM
	(
SELECT * FROM traitements.listing_gestionnaires_pln
UNION ALL
SELECT * FROM traitements.listing_gestionnaires_ppt
UNION ALL
SELECT * FROM traitements.listing_gestionnaires_psf
	) a
Group by a.espace, a.id, a.nom, a.gest_site
ORDER BY a.espace
)