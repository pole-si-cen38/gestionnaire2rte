-- Fait le ménage dans les schémas
DROP SCHEMA IF EXISTS rte CASCADE;
DROP SCHEMA IF EXISTS gestionnaires CASCADE;
--DROP SCHEMA IF EXISTS traitement CASCADE;
CREATE SCHEMA rte;
CREATE SCHEMA gestionnaires;