# <p align="Gestionnaires2rte">Title</p>


##  Contexte :
Dans le cadre d'une convention de travail avec RTE

<ins>**Objectifs :**</ins> Identifier les **gestionaires** où doit intervenir RTE afin d'échanger avec ces derniers en terme de **pratiques de gestion** et de **contraintes** sur les 

Identifier les sites d'intervention RTE qui croisent un ou des zonages dit "**gestionnaires**" afin de récupérer le **contact** de ce dernier mais également de **préconiser** des **mesures/pratiques de gestions** en fonction des **espèces et milieux**.

**Croiser les zonnages travaux RTE 2023** (shape: Point [2786], Ligne [597] et Polygon[4306]) avec les zonages environnementaux.
**Définir les zonages** : APPB,ZNIEFF,RNN,ENS,NATURA2000, PNR, ZH,PS, Comcom, communes, sp_patrimoniales (buffer) ...

Il peut y avoir certaines disparité d'information sur les "gestionnaires" retenu ou en fonction des départements (inventaire ZH, PS, Foêts...)

## Stats :

- 22 couches gestionnaires.
- 


##  🧐 Questions/Pb ?

- [ ] Suite à la réunion SI (17/11/2022), de nouveaux zonages sont intéressants à intégrer : Foret Public/?GEMAPI?/RBI/RBD

- [x]  Quels sources prendre pour les flux WFS gestionnaire :
```markdown
[IGN] https://wxs.ign.fr/environnement/geoportail/wfs?VERSION=2.0.0
issue de : 
https://geoservices.ign.fr/documentation/services/utilisation-sig/tutoriel-qgis/wfs
OU
[INPN] http://ws.carmencarto.fr/WFS/119/fxx_inpn?
issue de :
https://inpn.mnhn.fr/telechargement/cartes-et-information-geographique/nat/natura
```
Envoie mail (20/10/2022) à l'IGN pour savoir quel source de données prendre et qui maintient la mise à jour (données les plus à jour): https://geoservices.ign.fr/contact et https://inpn.mnhn.fr/contact/contacteznous
**<span style="color:green">Réponse mail IGN (20/10/22) :</span>** la donnée est produite par le MNHN et l'INPN donc c'est leurs données qu'il faut prendre en compte. L'IGN récupére leurs données pour les diffuser.

- [x] mail (20/10/22) INPN car il y a un pb avec la couche "Reserves_nationales_de_chasse_et_faune_sauvage" : correction effectuée le 7/11/2022

- [ ] mail (8/11/2022) à aspic-banatic@interieur.gouv.fr pour les données EPCI, problème de projection et encodage des caractères.
Non à jour : https://static.data.gouv.fr/resources/limites-des-epci-en-france-metropolitaine-en-2020/20200502-101247/epci-shapefile.zip
version 2022 : https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document\_reference/contours\_epci.zip


**!! Attention regarder comment l'intégration des couches (flux = 0) vides réagissent à l'export en shp ???**

## Echanges
**Réunion SI (17/11/2022) :**
De nouveaux zonages sont intéressants à intégrer : 
    - [x] Foret Public
    - [ ] GEMAPI
    - [x] RBI/RBD

http://ws.carmencarto.fr/WFS/105/ONF_Forets? 

**Marion De Groot / CEN 74 (7/11/2022) :**
Identifier les gestionnaires et soumettre des préconisation de gestions là où RTE doit intervenir.
Pour l'instant pas possible d'effectuer le travail en l'état, a demandé à RTE de faire des grands ensembles pour diminuer le nombre de lignes.
Prise en compte des "**données patrimonialités**" (buffer autour des sp à enjeux)
et **Communauté de commune** : https://www.data.gouv.fr/fr/datasets/limites-des-epci-en-france-metropolitaine-en-2020/ pour une transmission à ces derniers pour information.

## TODOLIST
A basculer dans le Kanban Framagit

## 🛠️ Tech Stack Tools
<ins>Environement de travail GNU/Linux </ins>
- Bash version > 5.0.17
- GDAL >3.0.4 ou utilisation au travers de docker
- Docker > 20.10.7 : pour la création d'un container postgis pour la manipulation des données spatial
- IDE : mise en forme des fichiers xml (coloration syntaxique)
- Excalidraw : schema
- Zettlr : Mise en forme Markdown (rédaction de la documentation)
- Git : versionnement du code
- Gitlab : .stocker le code
                .zutilisation tableau (Kanban)

## ## [](https://readmi.xyz/editor#-acknowledgements)🙇 Work Flow
Principe KISS : favoriser la simplicité…

![rte2gestionnaire_portrait.png](images/rte2gestionnaire_portrait.png)


Le travail avec les flux à l'échelle national permets une mutualisation des sources de données à l'échelle AURA.

1/ Création d'un **xml** avec l'ensemble des couches des gestionnaires


|          **Zonage**         	|                             **Source**                            	|
|:---------------------------:	|:-----------------------------------------------------------------:	|
| APPB                        	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| PNR                         	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| PN                          	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| znieff1                     	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| znieff2                     	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| réserve biologique          	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| Natura 2000 zps             	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| Natura 2000 SIC             	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| ZICO                        	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| RNR                         	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| RNN                         	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| RNCFS                         | http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| CEN_MU                      	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| Terrains acquis par les CEN 	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| Sites Ramsar 	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| ZICO	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| N2000_SIC 	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| N2000_ZPS	| http://ws.carmencarto.fr/WFS/119/fxx_inpn?                        	|
| ZH                          	| http://ws.carmencarto.fr/WFS/82/INV_ZH_2019_AVENIR_MAJ_02_2019?   	|
| PS                          	| http://ws.carmencarto.fr/WFS/82/CEN38_agregation_pelouses_seches? 	|
| ENS_ZO_01                   	| http://ws.carmencarto.fr/WFS/94/ENS_CG38?                         	|
| ENS_ZO_02                   	| http://ws.carmencarto.fr/WFS/94/ENS_CG38?                         	|
| EPCI             	| |
| Parcelles publique ONF             	 | http://ws.carmencarto.fr/WFS/105/ONF_Forets? |
| Forêt publique ONF        	| http://ws.carmencarto.fr/WFS/105/ONF_Forets? |
| Réserves Biologiques ONF                	| http://ws.carmencarto.fr/WFS/105/ONF_Forets? |
| Commune ?                	| |
…




#### Harmonisation et structure des tables gestionnaires

| id | nom | gest_site |
|:--:|:---:|:---------:|
|    |     |           |
|    |     |           |
|    |     |           |


- Filtre sur l'emprise d'intervention de RTE permet d'afficher uniquement notre territoire d'étude
exemple à adapter en fonction de l'emprise :` <SrcRegion clip="true">POLYGON((...))</SrcRegion>`

    - Emprise minimum BBOX() 5 points :` POLYGON((868033.1054999977350235 6417079.8662000000476837, 942612.1247999966144562 6417079.8662000000476837, 942612.1247999966144562 6491799.0267000012099743, 868033.1054999977350235 6491799.0267000012099743, 868033.1054999977350235 6417079.8662000000476837))`
    - Enveloppe minimum (convexHull) ~ 15 points : 

    création emprise minimal (convexe hull) de toutes les couches (./[]) travaux RTE
    
```bash
ogr2ogr convex_hull.shp points.shp -dialect SQLite -sql "SELECT ST_ConvexHull(ST_Collect(geometry)) FROM points"
```


## Choix
- Calcul du temps de traitement entre bbox (5 points) contre l'enveloppe minimum (15 points) :

Pour un total de 21 flux (couches) max ~ 1,6 secondes par couche

--→ avec bbox (4 points) : 47,30,41,29,31,30 secondes : moyenne 34,66 s
**– avec convexHull (poly ~ 15 points) 29, 28,31,31,27,29 secondes : moyenne 29.1 s**

...
2/ Enregistrer les couches gestionnaires shape dans un répertoire issue du fichier xml

Effectuer des traitements spatiales avec des flux est long, c'est pourquoi on va récupérer les couches d'information au format shp.
L'intégration direct des couches issues des flux directement dans Postgres génère des erreurs, c'est pourquoi je passe par une étape intermédiaire en transformant les flux en les shape et là c'est très rapide.

```bash
ogr2ogr --debug ON -f "ESRI Shapefile" gestionnaires_verbeux liste_flux_wfs_field.xml -lco SPATIAL_INDEX=YES 2> debug.txt
```
`2> : envoie la sortie des erreurs dans un fichiers txt pour analyser le traitement`

temps de traitement 29 secondes

 `**gestionnaires** `: création d'un répertoire accueillant tous les shapes
`-lco SPATIAL_INDEX=YES` : ajout d'un fichier .qix pour un index spatial 
 
3/ Transférer les couches **Shp** d'un répertoire vers une base **postgres** depuis le script bash

Bash -→ Linux
```bash
#! /bin/bash
# - script pour convertir les shp d'un répertoire vers une base postgres.
 
for file in `ls gestionnaires/*.shp`   
do ogr2ogr --debug ON -lco GEOMETRY_NAME=geom -overwrite -progress -f "PostgreSQL" PG:"host=192.168.0.81 port=5456 user=postgres password=1234 dbname=postgres active_schema=gestionnaires" $file -nlt MULTIPOLYGON -lco precision=NO 2> debug_gestionnaire.txt  
done
```
`-lco GEOMETRY_NAME=geom change le nom  de la colonne geom`


Batch → Windows
```powershell
FOR %Q IN (*.shp) DO ogr2ogr -overwrite -progress -f "PostgreSQL" PG:"host=192.168.0.81 port=5456 user=postgres password=1234 dbname=postgres active_schema=gestionnaire" "%Q" -lco ENCODING=UTF-8 -nlt MULTIPOLYGON -lco precision=NO`
```

**OU en GPKG**

```bash
#! /bin/bash
# script permettant de convertir les shp d'un répertoire vers un geopackage

for file in `ls -1 *.shp | sed -e 's/\.shp$//'` # Lister les couches shp en enelevant.
do ogr2ogr -overwrite -append -progress -f "GPKG" outp_gpkg4.gpkg $file.shp -nlt PROMOTE_TO_MULTI -nln  $file
done

```

4/ Transférer les couches RTE contenu dans un **zip** vers **postgres**

```bash
#! /bin/bash   
  
ogr2ogr --debug ON -lco GEOMETRY_NAME=geom -overwrite   -progress   -f   "PostgreSQL" PG:"host=192.168.0.81 port=5456 user=postgres password=1234 dbname=postgres active_schema=rte" /vsizip/./RTE.zip -nlt PROMOTE\_TO\_MULTI -lco  precision=NO 2> zip2pg_debug.txt
```

**OU en GPKG** dans un geopackage :

```bash
ogr2ogr  -overwrite -append -progress -f "GPKG" outp_gpkg4.gpkg /vsizip/../RTE.zip -nlt PROMOTE_TO_MULTI
```

Requête **Postgres** pour avoir la synthèse des sites et des gestionnaires :
 
## Automatisation

Script bash



### psql

Récupérer la liste des tables d'un schéma :
```bash
PGPASSWORD=1234 psql -h localhost -p 5456 -d postgres -U postgres -c '\\dt+ gestionnaires.*' | cut -d '|' -f2 | tail -n +4 | head -n -2
```

## Ressources

https://stackoverflow.com/questions/50826332/convert-tables-in-postgresql-to-shapefile
https://portailsig.org/content/ogr-que-la-force-soit-avec-les-formats-virtuels.html
https://gdal.org/drivers/vector/vrt.html
[geotribu](https://static.geotribu.fr/articles/2021/2021-09-07_traiter_fichiers_adresse_gdal_csv_vrt/#un-vrt-pour-les-donnees-de-la-ban)
iconv : https://opensharing.fr/commandes-linux-iconv
encodage : http://www.geoinformations.developpement-durable.gouv.fr/qgis-2-2-encodage-des-fichiers-shp-a2908.html

Excalidraw :

Utilisation de gdal à travers docker : https://tannergeo.com/2017/10/05/Use-GDAL-With-Docker.html

Utilisation de Gdal au travers de docker :
```bash
docker run -it --rm -v /Users/tannergeo/Documents/mydata:/root/mydata/ osgeo/gdal:ubuntu-small-3.6.0 /bin/bash
```
* `-it` \- interactive ttyl
* `--rm` \- remove the container after we exit it
* `-v` \- the volume to mount _/from/host/_ : _/on/container_
```bash
docker run --name my_pgadmin4 -p 88:80 \                                                     
    -e 'PGADMIN_DEFAULT_EMAIL=david.michallet@cen-isere.org' \
    -e 'PGADMIN_DEFAULT_PASSWORD=1234' \
    -e 'PGADMIN_CONFIG_ENHANCED_COOKIE_PROTECTION=True' \
    -e 'PGADMIN_CONFIG_LOGIN_BANNER="Authorised users only!"' \
    -e 'PGADMIN_CONFIG_CONSOLE_LOG_LEVEL=10' \
    -d dpage/pgadmin4
```

Exemple structuration de vrt -→ A Finir
```xml
<OGRVRTDataSource>
    <OGRVRTLayer name="rte38_gestionnaires">
        <SrcDataSource relativeToVRT="1">rte38_gestionnaires.csv</SrcDataSource>
        <!-- <SrcSQL dialect="sqlite">SELECT * FROM rte38_gestionnaires</SrcSQL> -->
        <Field name="espace" src="espace" type="String(10)" nullable="true"/>
	<Field name="id" src="id" type="Date(155)" nullable="true"/>
	<Field name="nom" src="nom" type="Integer" nullable="true"/>
	<GeometryField encoding="WKT" name="gestionnaire" field="gestionnaire"/>
    </OGRVRTLayer>
</OGRVRTDataSource>
```
- [ ] [script] fonction pour tester si tous les programmes sont présents.
`pg_isready -d postgres -h localhost -p 5456 -U postgres`

## ❤️ Support
david.michallet@cen-isere.org
## ➤ License

Distributed under the MIT License. See [LICENSE](https://readmi.xyz/LICENSE) for more information.
## ANNEXE
Création une enveloppe convex en sql

```sql
	Create table rte.convexhull_aggreg_rte as
(
with
pln as
(	
SELECT '1' as id, ST_ConvexHull(ST_Collect(a.wkb_geometry)) as geom FROM rte_pln_23_24_cen38 a
),
ppt as
(
SELECT '2' as id, ST_ConvexHull(ST_Collect(a.wkb_geometry)) as geom FROM rte_ppt_23_24_cen38 a
),
psf as
(
SELECT '3' as id, ST_ConvexHull(ST_Collect(a.wkb_geometry)) as geom FROM rte_psf_23_24_cen38 a
)

	Select ST_AsText(ST_ConvexHull(ST_Collect(st_union(st_union(ppt.geom,pln.geom),psf.geom)))) as geom
	FROM ppt, pln, psf
)

```

